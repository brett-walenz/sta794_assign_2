# Brett Walenz 
# STA 794 Record Linkage

This is a branch of the original reclink code found on bitbucket.
I implemented all the tasks in Python. There is a script that should execute everything (provided you are on a Mac/Linux box) in start.sh.

If you get a Python package error there is a requirements.txt file that lists the python packages required. I think it can be run by "pip -r requirements.txt".
I only need a few packages: jellyfish (for jarowinkler), pandas, sklearn. 

I couldn't find a Python implementation of Fellegi Sunter and I didn't have time to switch to R, so I went with Naive Bayes instead.
