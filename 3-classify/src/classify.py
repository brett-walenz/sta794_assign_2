import pandas as pd
import csv

from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import train_test_split
from sklearn.metrics import precision_score, recall_score
from sklearn.naive_bayes import GaussianNB
def dump_proba(outfile, X_test, y_test, probs):
    with open(outfile, 'w') as output:
        writer = csv.writer(output, delimiter='|')
        writer.writerow(["idx1", "idx2", "prob", "match"])
        for x, y, p in zip(X_test, y_test, probs):
            writer.writerow([x[0], x[1], y, p])


def classify(infile, outforest, outnaive):
    print("Starting classification task..")
    frame = pd.read_csv(infile, delimiter='|')
    X = frame.values[:,:-1]
    y = frame.values[:,-1]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

    forest = RandomForestClassifier()
    forest = forest.fit(X_train, y_train)
    forest_pred = forest.predict(X_test)
    forest_probs = forest.predict_proba(X_test)
    forest_precision = precision_score(y_test, forest_pred)
    forest_recall = recall_score(y_test, forest_pred)
    print("Trained and tested a random forest.")
    print("Precision: {0}".format(forest_precision))
    print("Recall: {0}".format(forest_recall))
    dump_proba(outforest, X_test, y_test, forest_probs)

    sgd = GaussianNB()
    sgd = sgd.fit(X_train, y_train)
    sgd_pred = sgd.predict(X_test)
    sgd_proba = sgd.predict_proba(X_test)
    sgd_precision = precision_score(y_test, sgd_pred)
    sgd_recall = recall_score(y_test, sgd_pred)
    print("Trained and tested a Naive Bayes classifier.")
    print("Precision: {0}".format(sgd_precision))
    print("Recall: {0}".format(sgd_recall))
    dump_proba(outnaive, X_test, y_test, sgd_proba)
print("Training on pairs-all-comparisons")
classify('../input/pairs-all-comparisons.csv', '../output/pairs-all-comparisons-forest.csv', '../output/pairs-all-comparisons-naive.csv')
print("=================================")

print("Training on determined comparisons")
classify('../input/pairs-determined-comparisons.csv', '../output/pairs-determined-comparisons-forest.csv', '../output/pairs-determined-comparisons-naive.csv')
print("=================================")

print("Training on soundex comparisons")
classify('../input/pairs-soundex-comparisons.csv', '../output/pairs-soundex-comparisons-forest.csv', '../output/pairs-soundex-comparisons-naive.csv')
print("=================================")


