echo "Performing blocking (all pairs, soundex, determined)"
(cd 1-blocking/src; python pairs-all-comparisons.py)
(cd 1-blocking/src; python pairs-soundex-comparisons.py)
(cd 1-blocking/src; python pairs-determined-comparisons.py)
cp 1-blocking/output/*.* 2-feature-generation/input
echo "...done."
echo "Starting feature generation."
(cd 2-feature-generation/src; python gen_features.py)
cp 2-feature-generation/output/*.* 3-classify/input
echo "..done."
echo "Starting classification (random forests, naive bayes)"
(cd 3-classify/src; python classify.py)
echo "..done"
echo "All tasks complete. Thank you for flying United."

