
import csv
from collections import defaultdict

def get_soundex(name):
    """Get the soundex code for the string
    Brett Walenz note: this implementation is from https://aprogrammerslife.wordpress.com/2010/02/14/an-implementation-of-the-soundex-algorithm-in-python/"""
    name = name.upper()

    soundex = ""
    soundex += name[0]

    dictionary = {"BFPV": "1", "CGJKQSXZ":"2", "DT":"3", "L":"4", "MN":"5", "R":"6", "AEIOUHWY":"."}

    for char in name[1:]:
        for key in dictionary.keys():
            if char in key:
                code = dictionary[key]
                if code != soundex[-1]:
                    soundex += code

    soundex = soundex.replace(".", "")
    soundex = soundex[:4].ljust(4, "0")

    return soundex


join = []
blocks = defaultdict(list)
blockid = 0

with open('../input/rldata500.csv', 'r') as infile:
    reader = csv.DictReader(infile, delimiter='|')
    #record.id|cluster.id|fname_c1|lname_c1|by|bm|bd
    for line in reader:
        blocks[get_soundex(line['lname_c1'])].append(line)

with open('../output/pairs-soundex-comparisons.py', 'w') as outfile:
    writer = csv.writer(outfile, delimiter='|')
    writer.writerow(['blockid', 'lid', 'rid', 'match'])
    for key in blocks:
        recs = blocks[key]
        for r in recs:
            for s in recs:
                val = 0
                if r['cluster.id'] == s['cluster.id']:
                    val = 1
                if int(r['record.id']) < int(s['record.id']):
                    writer.writerow((blockid, r['record.id'], s['record.id'], val))
        blockid = blockid + 1





