import csv
from collections import defaultdict

join = []
blocks = defaultdict(list)
blockid = 0

with open('../input/rldata500.csv', 'r') as infile:
    reader = csv.DictReader(infile, delimiter='|')
    #record.id|cluster.id|fname_c1|lname_c1|by|bm|bd
    for line in reader:
        blocks[line['by']].append(line)

with open('../output/pairs-determined-comparisons.py', 'w') as outfile:
    writer = csv.writer(outfile, delimiter='|')
    writer.writerow(['blockid', 'lid', 'rid', 'match'])
    for key in blocks:
        recs = blocks[key]
        for r in recs:
            for s in recs:
                val = 0
                if r['cluster.id'] == s['cluster.id']:
                    val = 1
                if int(r['record.id']) < int(s['record.id']):
                    writer.writerow((blockid, r['record.id'], s['record.id'], val))
        blockid = blockid + 1


