import csv

#record.id|cluster.id|fname_c1|lname_c1|by|bm|bd
records = []
join = []
blockid = 0
with open('../input/rldata500.csv', 'r') as infile:
    reader = csv.DictReader(infile, delimiter='|')
    #this nested loop works because the records are sorted in input
    for line in reader:
        for record in records: 
            val = 0
            if line['cluster.id'] == record['cluster.id']:
                val = 1
            join.append((blockid, line['record.id'], record['record.id'], val))
        records.append(line)

with open('../output/pairs-all-comparisons.csv', 'w') as outfile:
    writer = csv.writer(outfile, delimiter='|')
    writer.writerow(['blockid', 'lid', 'rid', 'match'])
    for entry in join:
        writer.writerow(entry)

