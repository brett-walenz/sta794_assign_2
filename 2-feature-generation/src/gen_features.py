import jellyfish
import csv
import glob
import os

#record.id|cluster.id|fname_c1|lname_c1|by|bm|bd
records = dict()
with open('../input/rldata500.csv', 'r') as infile:
    reader = csv.DictReader(infile, delimiter='|')
    #this nested loop works because the records are sorted in input
    for line in reader:
        records[line['record.id']] = line

inputs = glob.glob('../input/pairs*.csv')
features = ['fname_c1', 'lname_c1', 'by', 'bm', 'bd']
for i in inputs:
    outputs = []
    with open(i, 'r') as matchlist:
        reader = csv.reader(matchlist, delimiter='|') 
        next(reader)
        for line in reader:
            #blockid|idx1|idx2|match
            idx1 = line[1]
            idx2 = line[2]
            output = [idx1, idx2]
            match = int(line[3])
            rec1 = records[idx1]
            rec2 = records[idx2]
            for f in features:
                s = jellyfish.jaro_winkler(rec1[f], rec2[f])
                output.append(s)
            output.append(match)
            outputs.append(output)
    basefile = os.path.basename(i)
    with open('../output/' + basefile, 'w') as outfeatures:
        writer = csv.writer(outfeatures, delimiter='|')
        header = ['idx1', 'idx2']
        header.extend(features)
        header.append('match')
        writer.writerow(header)
        for o in outputs:
            writer.writerow(o)
            
    
